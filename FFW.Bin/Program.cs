﻿// FastFrameWork 快速开发框架
// 创建时间：2015-06-23 22:56:33 
// 创建作者：穆建情
// 联系方式：66767376@QQ.Com
// 技术博客:http://www.cnblogs.com/MuNet/category/856588.html
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using System.Threading;
using System.Text;

namespace FFW.Bin
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //注册全局异常捕捉
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.ThreadException += new ThreadExceptionEventHandler(UIThreadException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledException);

            //应用皮肤
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            BonusSkins.Register();
            SkinManager.EnableFormSkins();
            UserLookAndFeel.Default.SetSkinStyle("DevExpress Style");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("zh-Hans");

            Application.Run(new RibbonForm());

        }

        private static void UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            FFW.Utilitys.WaitControlEx.Close();
            var error = GetExceptionMsg((Exception)e.ExceptionObject);
            XtraMessageBox.Show(error, "系统异常", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private static void UIThreadException(object sender, ThreadExceptionEventArgs t)
        {
            FFW.Utilitys.WaitControlEx.Close();
            var error = GetExceptionMsg(t.Exception);
            XtraMessageBox.Show(error, "程序异常", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private static string GetExceptionMsg(Exception ex)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("****************************异常信息****************************");
            sb.AppendLine("【出现时间】：" + DateTime.Now.ToString());
            if (ex != null)
            {
                sb.AppendLine("【异常类型】：" + ex.GetType().Name);
                sb.AppendLine("【异常信息】：" + ex.Message);
                sb.AppendLine("【堆栈调用】：" + ex.StackTrace);
            }
            else
            {
                sb.AppendLine("【异常信息为空】");
            }
            sb.AppendLine("***************************************************************");
            return sb.ToString();
        }
    }
}
