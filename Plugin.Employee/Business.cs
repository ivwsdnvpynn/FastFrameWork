﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Plugin.Employee
{
    //FFW.Data 数据库访问类库具体使用方法请参考 http://www.cnblogs.com/MuNet/p/5740833.html
    public class Business
    {
        public static DataTable GetAllGrade()
        {
            var context = new FFW.Data.DataContext();
            //方式1
            //DataTable grades = context.Builder<DataTable>().Select("ID,Class,Level").From("Grade").QuerySingle();
            //方式2
            DataTable grades = context.Script("Select ID,Class,Level From Grade").QuerySingle<DataTable>();
            return grades;
        }
    }
}
